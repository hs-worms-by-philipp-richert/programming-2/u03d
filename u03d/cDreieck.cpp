#include "cDreieck.h"

// Konstruktor
cDreieck::cDreieck(cPunkt a_in, cPunkt b_in, cPunkt c_in) : punktA(a_in), punktB(b_in), punktC(c_in) {
	ausgabe();
}

// Brechnet Dreiecks-Umfang
double cDreieck::umfangD() {
	double ab = getDistance(punktA, punktB);
	double bc = getDistance(punktB, punktC);
	double ac = getDistance(punktA, punktC);
	return ab + bc + ac;
}

// Berechnet Dreiecks-Flaeche
double cDreieck::flaecheD() {
	double s = umfangD() / 2;
	double ab = getDistance(punktA, punktB);
	double bc = getDistance(punktB, punktC);
	double ac = getDistance(punktA, punktC);
	double area = sqrt(s*(s - ab)*(s - bc)*(s - ac));

	return area;
}

// berechnet Distanz zwischen zwei Punkten
double cDreieck::getDistance(cPunkt punkt1, cPunkt punkt2) {
	double d = sqrt(pow(punkt2.getX() - punkt1.getX(), 2) + pow(punkt2.getY() - punkt1.getY(), 2));
	return d;
}

// Ausgabefunktion
void cDreieck::ausgabe() {
	cout << "Dreieck:" << endl
		<< "\tPunkt A: ";
	punktA.ausgabe();

	cout << "\tPunkt B: ";
	punktB.ausgabe();

	cout << "\tPunkt C: ";
	punktC.ausgabe();

	cout << "\tUmfang: " << umfangD() << endl
		<< "\tFlaeche: " << flaecheD() << endl << endl;
}