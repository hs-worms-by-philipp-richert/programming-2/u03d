#pragma once

#include "cPunkt.h"
#include<iostream>
#include<cmath>

class cDreieck
{
private:
	cPunkt punktA;
	cPunkt punktB;
	cPunkt punktC;
	double umfangD();
	double flaecheD();
	double getDistance(cPunkt, cPunkt);
public:
	cDreieck(cPunkt = cPunkt(0.0, 1.0), cPunkt = cPunkt(1.0, 0.0), cPunkt = cPunkt(0.0, 0.0));
	void ausgabe();
};

