#include "cPunkt.h"

// Konstruktor
cPunkt::cPunkt(double x_in, double y_in) {
	// ueberschreibe Werte kleiner -10 mit -10
	// und Werte groesser 10 mit 10:
	coordX = max(-10.0, min(10.0, x_in));
	coordY = max(-10.0, min(10.0, y_in));;
}

// Ausgabefunktion
void cPunkt::ausgabe() {
	cout << "x = " << coordX << "; y = " << coordY << endl;
}

// getter fuer x
double cPunkt::getX() {
	return coordX;
}

// getter fuer y
double cPunkt::getY() {
	return coordY;
}