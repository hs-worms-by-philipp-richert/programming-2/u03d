// Aufgabe u03d
// Programm zur Dreiecksberechnung
// Philipp Richert
// 03.11.2022

#include "cDreieck.h"

int main() {
	cDreieck dreiecke[3] = { {{23.9, 3.13}, {5.24, -16.8}, {-6.72, 8.42}}, {{0.5, 1.0}, {1.5, 0.0}, {0.5, 0.0}} };

	// Ich habe die ausgabe() methode direkt im Konstruktor aufgerufen
	// alternativ ginge es aber auch so:
	/*int size = sizeof(dreiecke) / sizeof(*dreiecke);

	for (int i = 0; i < size; i++) {
		dreiecke[i].ausgabe();
	}*/

	return 0;
}